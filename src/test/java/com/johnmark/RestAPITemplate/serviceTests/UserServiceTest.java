package com.johnmark.RestAPITemplate.serviceTests;

import com.johnmark.RestAPITemplate.dto.ErrorMessage;
import com.johnmark.RestAPITemplate.dto.UserDTO;
import com.johnmark.RestAPITemplate.models.TeamModel;
import com.johnmark.RestAPITemplate.models.UserModel;
import com.johnmark.RestAPITemplate.models.UserRole;
import com.johnmark.RestAPITemplate.repositories.TeamRepository;
import com.johnmark.RestAPITemplate.repositories.UserRepository;
import com.johnmark.RestAPITemplate.services.TeamModelService;
import com.johnmark.RestAPITemplate.services.UserModelService;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class UserServiceTest {

    @Autowired
    private UserModelService userModelService;

    @Autowired
    private TeamModelService teamModelService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Test
    public void testRegisterNewUser() {
        final Date date = new Date();
        final UserDTO userDTO = new UserDTO("username", "password", "email@email.com", date);
        final Validation<ErrorMessage, UserModel> registerUser = userModelService.registerUser(userDTO);
        assertTrue(registerUser.isValid());
        final UserModel userModel = registerUser.get();
        assertEquals("username", userModel.getUsername());
        assertEquals("email@email.com", userModel.getEmail());
        assertEquals(date, userModel.getDateOfBirth());
        assertEquals(UserRole.REGULAR, userModel.getUserRole());
        passwordEncoder.matches("password", userModel.getPassword());

        final Optional<UserModel> byId = userRepository.findById(1L);

        assertTrue(byId.isPresent());
        assertEquals("username", byId.get().getUsername());
    }

    @Test
    public void testRegisterNewUserAndGiveThemTeam() {
        final Date date = new Date();
        final UserDTO userDTO = new UserDTO("username", "password", "email@email.com", date);
        final Validation<ErrorMessage, UserModel> registerUser = userModelService.registerUser(userDTO);
        final UserModel userModel = registerUser.get();
        assertEquals("username", userModel.getUsername());
        assertEquals("email@email.com", userModel.getEmail());
        assertEquals(date, userModel.getDateOfBirth());
        assertEquals(UserRole.REGULAR, userModel.getUserRole());
        passwordEncoder.matches("password", userModel.getPassword());

        final TeamModel teamModel = new TeamModel("T1");
        teamRepository.save(teamModel);

        final Optional<UserModel> byId = userRepository.findById(1L);

        assertTrue(byId.isPresent());
        final UserModel getUser = byId.get();
        teamModelService.addUserToTeam(teamModel.getId(), getUser);

        final Optional<TeamModel> byId1 = teamRepository.findById(1L);

        final List<UserModel> t1 = byId1.get().getUsers().stream().filter(userModel1 -> userModel1.getTeam().getName().equals("T1")).toList();
        assertEquals(1, t1.size());
    }

    @Test
    public void testRegisterUserThenRegisterOneSameUsername() {
        final Date date = new Date();
        final UserDTO userDTO = new UserDTO("username", "password", "email@email.com", date);
        final Validation<ErrorMessage, UserModel> registerUser = userModelService.registerUser(userDTO);
        assertTrue(registerUser.isValid());
        final Validation<ErrorMessage, UserModel> registerUser2 = userModelService.registerUser(userDTO);
        assertFalse(registerUser2.isValid());
    }

    @Test
    public void testRegisterUserThenRegisterOneSameEmail() {
        final Date date = new Date();
        final UserDTO userDTO = new UserDTO("username", "password", "email@email.com", date);
        final Validation<ErrorMessage, UserModel> registerUser = userModelService.registerUser(userDTO);
        assertTrue(registerUser.isValid());
        final UserDTO userDTO2 = new UserDTO("username2", "password", "email@email.com", date);
        final Validation<ErrorMessage, UserModel> registerUser2 = userModelService.registerUser(userDTO2);
        assertFalse(registerUser2.isValid());
    }

    @Test
    public void testAddFriend() {
        addFriend();
        userRepository.findAll()
                .forEach(userModel -> assertTrue(userModel.getFriends().size() > 0));
    }

    private void addFriend() {
        final Date date = new Date();
        final UserDTO userDTO = new UserDTO("username", "password", "email@email.com", date);
        final UserDTO userDTO2 = new UserDTO("usernam2", "password", "email2@email.com", date);
        final Validation<ErrorMessage, UserModel> registerUser1 = userModelService.registerUser(userDTO);
        final Validation<ErrorMessage, UserModel> registerUser2 = userModelService.registerUser(userDTO2);
        assertTrue(registerUser1.isValid());
        assertTrue(registerUser2.isValid());
        final UserModel userModel1 = registerUser1.get();
        final UserModel userModel2 = registerUser2.get();
        userModelService.addFriend(userModel1.getId(), userModel2.getId());
    }

    @Test
    public void testRemoveFriend() {
        addFriend();
        userModelService.removeFriend(1L, 2L);
        userRepository.findAll()
                .forEach(userModel -> assertEquals(0, userModel.getFriends().size()));
    }

}
