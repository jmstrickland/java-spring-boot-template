package com.johnmark.RestAPITemplate.controllerTests;

import com.johnmark.RestAPITemplate.controllers.UserController;
import com.johnmark.RestAPITemplate.dto.UserDTO;
import com.johnmark.RestAPITemplate.repositories.TeamRepository;
import com.johnmark.RestAPITemplate.repositories.UserRepository;
import com.johnmark.RestAPITemplate.services.TeamModelService;
import com.johnmark.RestAPITemplate.services.UserModelService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = UserController.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserController userController;

    @MockBean
    private UserModelService userModelService;

    @MockBean
    private TeamModelService teamModelService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private TeamRepository teamRepository;

    @Test
    public void testControllerRegisterNewUser() throws Exception {
        UserDTO userDTO = new UserDTO("a", "b", "c", new Date());
        final ResultActions user = mockMvc.perform(post("/user/new_user").param("user", String.valueOf(userDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        System.out.println();

    }

}
