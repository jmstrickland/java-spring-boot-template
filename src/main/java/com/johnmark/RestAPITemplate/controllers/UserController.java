package com.johnmark.RestAPITemplate.controllers;

import com.johnmark.RestAPITemplate.dto.ErrorMessage;
import com.johnmark.RestAPITemplate.dto.ResponseEntity;
import com.johnmark.RestAPITemplate.dto.UserDTO;
import com.johnmark.RestAPITemplate.models.UserModel;
import com.johnmark.RestAPITemplate.services.UserModelService;
import io.vavr.control.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(name = "/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserModelService userModelService;

    @PostMapping(value = "/new_user")
    public ResponseEntity registerUser(@RequestParam(value = "user") UserDTO userDTO) {
        LOGGER.debug("Registering User: " + userDTO.toString());
        final Validation<ErrorMessage, UserModel> userModels = userModelService.registerUser(userDTO);
        if (userModels.isInvalid()) {
            LOGGER.debug(userModels.getError().toString());
            return new ResponseEntity().addError(userModels.getError().getErrorMessage());
        } else {
            LOGGER.debug("Registered User ID: " + userModels.get().getId());
            return new ResponseEntity(userModels.get());
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getUserById(@PathVariable Long id) {
        Validation<ErrorMessage, UserModel> userById = userModelService.getUserById(id);
        if (userById.isInvalid()) {
            return new ResponseEntity(userById.get());
        } else {
            return new ResponseEntity().addError(userById.getError().getErrorMessage());
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteUserById(@PathVariable Long id){
        final Validation<ErrorMessage, Boolean> validation = userModelService.deleteUserById(id);
        if(validation.isInvalid()){
            return new ResponseEntity().addError(validation.getError().getErrorMessage());
        }
        else{
            return new ResponseEntity(true);
        }
    }

}
