package com.johnmark.RestAPITemplate.dto;

import lombok.ToString;
import lombok.Value;

@Value
@ToString
public class ErrorMessage {

    public final static ErrorMessage USER_NOT_FOUND = new ErrorMessage("User cannot be found by ID", 404, "");

    String errorMessage;
    int errorCode;
    String debugMessage;
}
