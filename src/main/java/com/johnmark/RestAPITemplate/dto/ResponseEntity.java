package com.johnmark.RestAPITemplate.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class ResponseEntity {

    private Object responseObject;
    private boolean hasFailed;
    private String errorMessage;

    public ResponseEntity(Object responseObject) {
        this.responseObject = responseObject;
    }

    public ResponseEntity addError(String errorMessage){
        this.hasFailed = true;
        this.errorMessage = errorMessage;
        return this;
    }
}
