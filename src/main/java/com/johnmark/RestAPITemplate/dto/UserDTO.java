package com.johnmark.RestAPITemplate.dto;

import lombok.ToString;
import lombok.Value;

import java.util.Date;

@Value
@ToString
public class UserDTO {
    String username;

    @ToString.Exclude String password;

    String email;

    Date dateOfBirth;

}
