package com.johnmark.RestAPITemplate.repositories;

import com.johnmark.RestAPITemplate.models.TeamModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<TeamModel, Long> {
}
