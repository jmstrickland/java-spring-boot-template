package com.johnmark.RestAPITemplate.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "aware")
public class AuditConfig {


    //    @Bean("auditorProvider")
//    public AuditorAware<String> auditorProvider() {
//        return new AuditorAwareImpl();
//    }
    @Bean
    public AuditorAware<String> aware() {
        return () -> Optional.of("Administrator");
    }
}
