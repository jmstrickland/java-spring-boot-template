package com.johnmark.RestAPITemplate.models;

public enum UserRole {
    ADMIN("Has access to every piece of the application"),
    REGULAR("Has limited access to every piece of the application");

    UserRole(String description) {
    }
}
