package com.johnmark.RestAPITemplate.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    @NonNull
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "team_id")
    @With
    private Set<UserModel> users;
}
