package com.johnmark.RestAPITemplate.services;

import com.johnmark.RestAPITemplate.models.TeamModel;
import com.johnmark.RestAPITemplate.models.UserModel;
import com.johnmark.RestAPITemplate.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TeamModelService {

    @Autowired
    private TeamRepository teamRepository;

    public TeamModel addUserToTeam(Long teamId, UserModel userModel){
        final Optional<TeamModel> byId = teamRepository.findById(teamId);
        if(byId.isPresent()){
            final TeamModel teamModel = byId.get();
            teamModel.getUsers().add(userModel);
            teamRepository.save(teamModel);
        }
        else{
            return null;
        }
        return null;
    }
}
