package com.johnmark.RestAPITemplate.services;

import com.johnmark.RestAPITemplate.dto.ErrorMessage;
import com.johnmark.RestAPITemplate.dto.UserDTO;
import com.johnmark.RestAPITemplate.models.UserModel;
import com.johnmark.RestAPITemplate.repositories.UserRepository;
import io.vavr.control.Try;
import io.vavr.control.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.johnmark.RestAPITemplate.dto.ErrorMessage.USER_NOT_FOUND;

@Service
public class UserModelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserModelService.class);
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Validation<ErrorMessage, UserModel> registerUser(UserDTO userDTO){
        if(isEmailAlreadyUsed(userDTO.getEmail())){
            return Validation.invalid(new ErrorMessage("Email already used", 404, ""));
        }
        if(isUsernameAlreadyUsed(userDTO.getUsername())){
            return Validation.invalid(new ErrorMessage("Username already used", 404, ""));
        }

        final UserModel userModel = new UserModel(userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getEmail(),
                userDTO.getDateOfBirth());
        userRepository.save(userModel);
        return Validation.valid(userModel);
    }

    public Validation<ErrorMessage, UserModel> getUserById(Long id){
        final Optional<UserModel> userModelOptional = userRepository.findById(id);

        if(userModelOptional.isPresent()){
            return Validation.valid(userModelOptional.get());
        }
        else{
            LOGGER.debug("Cannot find user with ID: " + id);
            return Validation.invalid(new ErrorMessage("Cannot find user by ID", 405, "No User"));
        }
    }

    public Validation<ErrorMessage, Boolean> deleteUserById(Long id){
        final Try<Void> run = Try.run(() -> userRepository.deleteById(id));
        if(run.isFailure()){
            return Validation.invalid(USER_NOT_FOUND);
        }
        else{
            return Validation.valid(true);
        }
    }

    public Validation<ErrorMessage, UserModel> addFriend(Long id1, Long id2){
        // Chekc that friend isnt already there
        if(Objects.equals(id1, id2)){
            return Validation.invalid(new ErrorMessage("ID's are the same", 404, ""));
        }
        final Optional<UserModel> userModelOptional = userRepository.findById(id1);
        final Optional<UserModel> userModel2Optional = userRepository.findById(id2);
        if(userModelOptional.isEmpty() || userModel2Optional.isEmpty()){
            return Validation.invalid(new ErrorMessage("One of the users cannot be found", 404, ""));
        }
        final UserModel userModel1 = userModelOptional.get();
        final UserModel userModel2 = userModel2Optional.get();
        userModel1.getFriends().add(userModel2);
        userModel2.getFriends().add(userModel1);
        userRepository.save(userModel1);
        return Validation.valid(userModel1);
    }

    public Validation<ErrorMessage, UserModel> removeFriend(Long id1, Long id2) {
        if(Objects.equals(id1, id2)){
            return Validation.invalid(new ErrorMessage("ID's are the same", 404, ""));
        }
        final Optional<UserModel> userModelOptional = userRepository.findById(id1);
        final Optional<UserModel> userModel2Optional = userRepository.findById(id2);
        if(userModelOptional.isEmpty() || userModel2Optional.isEmpty()){
            return Validation.invalid(new ErrorMessage("One of the users cannot be found", 404, ""));
        }
        final UserModel userModel1 = userModelOptional.get();
        final UserModel userModel2 = userModel2Optional.get();
        final Set<UserModel> stream = userModel1.getFriends().stream().filter(userModel -> !userModel.getId().equals(id2)).collect(Collectors.toSet());
        final Set<UserModel> stream2 = userModel2.getFriends().stream().filter(userModel -> !userModel.getId().equals(id1)).collect(Collectors.toSet());
        final UserModel withFriends = userModel1.withFriends(stream);
        final UserModel withFriends2 = userModel2.withFriends(stream2);
        userRepository.save(withFriends);
        userRepository.save(withFriends2);
        return Validation.valid(userModel1);
    }

    private boolean isEmailAlreadyUsed(String email){
        return userRepository.findByEmail(email)
                .isPresent();
    }

    private boolean isUsernameAlreadyUsed(String username){
        return userRepository.findByUsername(username)
                .isPresent();
    }
}
